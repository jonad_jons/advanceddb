import urllib2
import string
import nltk
import math
import random
from collections import Counter

#list of states
class Hmm_Model:
    def __init__(self, url):

        #different states of my training
        self.states = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
             'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
             'X', 'Y', 'Z']
        #Initial Corpus
        self.corpus = self.getFile(url)

        #testing corpus
        self.testingCorpus = self.getTestFile()

        #training corpus
        self.trainingCorpus = self.getTrainingFile()
        print len(self.trainingCorpus)

        #corrupted testing corpus at 10 percent
        self.corruptedTestingCorpus = self.corruptTest(self.getTestFile(), 10)

        #corrupted training corpus at 20 percent
        self.corruptedTrainingCorpus = self.corruptTest(self.getTrainingFile(), 20)

        #likelihood to be in a given states
        self.stateProbabilities = self.frequencyStates(self.trainingCorpus)

        #likelihood of being in a specific state and observe a specific letter
        self.observations = self.observationEstimation(self.trainingCorpus, self.corruptedTrainingCorpus)

        #start states probabilities likelihood to start in a given states
        self.startProb = self.frequencyStartStates(self.trainingCorpus)

        #get the corpus
    def getCorpus(self):
            return self.corpus

        #get the training corpus
    def getTrainingCorpus(self):
            return self.trainingCorpus


    def getStateProb(self):
        return self.stateProbabilities

    def getStates(self):
        return self.states

    #given a url download document and return a list of that document
    def getFile(self, url):
        corpus = []
        fil = urllib2.urlopen(url)
        textdoc = fil.read()
        lowers = textdoc.lower()
        no_punctuation = lowers.translate(None, string.punctuation)
        tokens = nltk.word_tokenize(no_punctuation)
        for i in tokens:
            l = i.translate(None, string.digits)
            corpus.append(l)
        corp = filter(None, corpus)
        return corp

    #take the first 20% of the corpus as a testing document
    def getTestFile(self):
        size = len(self.corpus)*0.2
        finalSize = int(size)
        return self.corpus[:finalSize]

    #assign the training set
    def getTrainingFile(self):
        size = len(self.getTestFile())
        return self.corpus[size:]

    #utility function that corrupt a text giving a percentage
    def corruptTest(self, input, percent):
        output = input
        numCorrupted =  int(math.ceil(len(input)*percent/100))
        i = 0
        while i < numCorrupted:
            randNum = random.randint(0,len(input) - 1)
            corruptArray = input[randNum]
            index = random.randint(0,25)
            coruptIndex = random.randint(0,len(corruptArray) -1)
            while self.states[index] == corruptArray[coruptIndex]:
                index = random.randint(0,25)
                coruptIndex = random.randint(0,len(corruptArray) -1)


            new_tup = corruptArray[:coruptIndex] + self.states[index].lower() + corruptArray[coruptIndex + 1:]
            i += 1
            output[randNum] = new_tup
        return output

    #giving the input compute the start state probabilities
    def frequencyStartStates(self, input):
        dictStates = {}
        size = len(input)
        for item in input:
            ch = item[0]
            try:
                dictStates[ch.upper()] += 1.0
            except:
                dictStates[ch.upper()] = 1.0
        for i in dictStates:
            dictStates[i] = float(float(dictStates[i])/float(size))
            dictStates[i] = float("{0:.4f}".format(round(dictStates[i],4)))
        for i in self.states:
            if i in dictStates:
                continue
            else:
                dictStates[i] = 0.000
        return dictStates

    #get the start frequencies
    def getStartFreq(self):
        return self.startProb
    #giving the input file compute the frequency state probability
    def frequencyStates(self, input):
        freqStates = {}
        for item in input:
            for i in item:
                try:
                    freqStates[i.upper()] += 1
                except:
                    freqStates[i.upper()] = 1

        size = sum(freqStates.values())
        for i in freqStates:
            freqStates[i] = float(float(freqStates[i])/float(size))
            freqStates[i] = float("{0:.4f}".format(round(freqStates[i],4)))
        return freqStates

    #get the observation
    def getObservation(self):
        return self.observations


    #method that return the observation given a state
    def observationEstimation(self, states, observations):
        tmpResult = {}
        result = {}
        for item1, item2 in zip(states, observations):
            item1 = item1.replace(" ", "")
            item2 = item2.replace(" ", "")
            for i1, i2 in zip(item1, item2):
                ind = i1.upper()
                try:
                    tmpResult[ind].append(i2)
                except:
                    tmpResult[ind] = [i2]
        for item in tmpResult:
            arr = tmpResult[item]
            size = len(arr)
            occu = dict(Counter(arr))
            for element in occu:
                occu[element] = float(occu[element])/float(size)
            for k in self.states:
                if k.lower() in occu:
                   continue
                else:
                    occu[k.lower()] = 0.0
            result[item] = occu
        for key in self.states:
            if key in result:
                continue
            else :
                result[key] = {}
        return result

    #return the corrupted testing corpus
    def getCorruptedTestingCorpus(self):
        return self.corruptedTestingCorpus
    #return the corrupted training corpus
    def getCorruptedTraining(self):
        return self.corruptedTrainingCorpus

    #return the testing corpus
    def getTestingCorpus(self):
        return self.testingCorpus

    #viterbi algorithm to find the hidden path
    def viterbi(self, obs, states, start_p, trans_p, emit_p):
        V = [{}]
        path = {}
        #initialize
        for y in states:
            V[0][y] = start_p[y] * emit_p[y][obs[0]]
            path[y] = [y]
        # Run Viterbi
        for t in range(1, len(obs)):
            V.append({})
            newpath = {}
            for y in states:
                (prob, state) = max((V[t-1][y0] * trans_p[y] * emit_p[y][obs[t]], y0) for y0 in states)
                V[t][y] = prob
                newpath[y] = path[state] + [y]
            path = newpath
        n = 0
        if len(obs)!=1:
            n = t
        (prob, state) = max((V[n][y], y) for y in states)
        return (prob, path[state])

    #write array in ouput
    def writeArrayOutput(self, name, input):
        file = open(name + ".txt", "w+")
        file.write("the output is \n\n")
        for element in input:
            file.write(element + "\n")
        file.close()

    #write dictionary in the file
    def writeDict(self, name, input):
        file = open(name + ".txt", "w+")
        file.write("the output is: " + "\n\n")
        for element in input:
            file.write(str(element) + " : " + str(input[element])  +"\n")
        file.close()




#function to run the testing
def test(hmm_model):
    #define the model

    #the hmm output
    hmmoutput = []
    out = []

    #for each element in the testing set, compute the viterbi algorithm
    for i in hmm_model.getCorruptedTestingCorpus():
        s = hmm_model.viterbi(i, hmm_model.getStates(), hmm_model.getStartFreq(), hmm_model.getStateProb(), hmm_model.getObservation())
        out.append(s)
        hmmoutput.append(i + " : " + str(hmm_model.viterbi(i, hmm_model.getStates(), hmm_model.getStartFreq(), hmm_model.getStateProb(), hmm_model.getObservation())))

    # all corrupted words:
    #total Error
    total = 0

    #total correction
    correction = 0

    #list of words in the testing corpus, in the corrupted testing corpus and in the hmm output
    corruptedWord = []

    #number of true correction
    trueCorrection = 0

    #end result, of precision and recal
    result = []

    #for each element in the testing corpus, corrupted corpus and hmm model, compute precision and recal
    for element1, element2, element3  in zip(hmm_model.getTestFile(), hmm_model.getCorruptedTestingCorpus(), out):
        newEl = ''.join(element3[1]).lower()
        if not(element1 is element2):
            total += 1
            if not(element2 is newEl):
                correction += 1
                if (newEl is element1):
                    trueCorrection+= 1
                    result.append(element1 + " " +  element2 + " " +  newEl)
            corruptedWord.append(str(element1) + " " + str(element2) + " " + newEl)
    result.append( "total Error " +  str(total))
    result.append("Total Correction " + str(trueCorrection))
    result.append("precision " + str(float(trueCorrection)/(float(correction))))
    result.append("recall " + str(float(trueCorrection)/float(total)))

    #output of the hmm function
    hmm_model.writeArrayOutput("hmmOuput", hmmoutput)

    #output of the list of states
    hmm_model.writeArrayOutput("States " , hmm_model.getStates())

    #output of the start probabilities
    hmm_model.writeDict("Start_States_Probabilities", hmm_model.getStartFreq())

    #output of the transition probabilities
    hmm_model.writeDict("Transition_Probabilities" , hmm_model.getStateProb())

    #output of the observation probabilities
    hmm_model.writeDict("Observations ", hmm_model.getObservation())

    #ouput of the Corpus used
    hmm_model.writeArrayOutput("Corpus ", hmm_model.getCorpus())

    #output of the corrupted corpus
    hmm_model.writeArrayOutput("Corrupted_Training_Corpus", hmm_model.getCorruptedTraining())

    #output of the corrupted testing corpus
    hmm_model.writeArrayOutput("Corrupted_Testing_Corpus", hmm_model.getCorruptedTestingCorpus())

    #output the list of words in the testing file with their corresponding corrupted word, and hmm result
    hmm_model.writeArrayOutput("Corrupted_word ", corruptedWord)

    #output the end Result
    hmm_model.writeArrayOutput("Result:  " , result)

#main function to evaluate a string
def main(hmm_model, search):
    s = hmm_model.viterbi(search, hmm_model.getStates(), hmm_model.getStartFreq(), hmm_model.getStateProb(), hmm_model.getObservation())
    return s


def choose():
    hmm_model = Hmm_Model('http://cyber.eserver.org/unabom.txt')
    run = True
    while run:
        print 'enter your option'
        search = raw_input('Enter the option \'t\' for testing and \'m\' to run the main \'q\' to quit ').strip()
        if search == 't':
            test(hmm_model)
        elif search == 'm':
            search = raw_input('Enter a string:m ').strip()
            s = main(hmm_model, search)
            print search + " : " + str(s)

        elif search == 'q':
            run = False


if __name__ == "__main__":
    choose()












