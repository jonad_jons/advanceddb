import nltk 
import string
import re
import os
from nltk.corpus import stopwords
from nltk.stem.porter import *
from nltk.stem import *
from collections import Counter
import sys
import errno

#utility function that take a file name as an input and return and a list of
#all document in the file
def getinput(input):
    with open(input) as documents:
        pattern = re.compile('.I\s\d+')
        textDoc = []
        tmpDoc = ''
        for line in documents:
            m = pattern.match(line.strip())
            if line.strip() == '.I 1':
                continue
            elif m:
                textDoc.append(tmpDoc)
                tmpDoc = ''
            else:
                tmpDoc  = tmpDoc + " " + line.strip()
    textDoc.append(tmpDoc)
    return textDoc
#utility function that take a list of string
# and  lower, remove punctuation and tokenize each string in the list.   
def get_tokens(input):
    result = []
    for item in input:
        lowers = item.lower()
        no_punctuation = lowers.translate(None, string.punctuation)
        tokens = nltk.word_tokenize(no_punctuation)
        result.append(tokens)
    return result

#utility function that take a list of string and remove stop words
def filter_tokens(list):
    return [w for w in list if not w in stopwords.words('english')]

#utility function to run the Porter stemmer
def run_stemmer(tokens):
    stemmer = PorterStemmer()
    stemmed = []
    newS = []
    for item in tokens:
        for term in item:
            newS.append(stemmer.stem(term))
        stemmed.append(newS)
        newS = []
    return stemmed

#utility function that take as an input a list of documents and 
#return a list of term : (document Id , term frequency)
def docFreq(input):
    tmpDic = {}
    arrDic = []
    newS = {}
    for i in input:
        for item in i:
            try:
                tmpDic[item] += 1
            except:
                tmpDic[item] = 1
        for key in tmpDic.keys():
            newS[key] = (input.index(i) + 1, tmpDic[key])
        arrDic.append(newS)
        newS = {}
        tmpDic = {}
    return arrDic

#utility function that create a a map of term : (document frequency, (document : term frequency))
def createMap(input):
    res = {}
    for item in input:
        for key in item:
            try :
                doc = res[key][0] + 1
                tmp = res[key][1]
                tmp.append(item[key])
                res[key] = (doc, tmp)
            except :
                res[key] = (1, [item[key]])
    return res

#utility function that take a list and create a file output for assignment 2
def writeOutput(input):
    file = open("INV_FILE_HASH" + ".txt", "w+")
    file.write("INV_FILE_HASH  = { " +  "\n")
    for element in input.keys():
        file.write("'" + element + "'" + " => "  +  str(input[element]) +  '\n')
    file.close()


def main():
    filename = os.path.abspath(sys.argv[1])
    oput = run_stemmer(filter_tokens(get_tokens(getinput(filename))))
    docfre = createMap(docFreq(oput))
    writeOutput(docfre)

if __name__ == '__main__':
    main()

