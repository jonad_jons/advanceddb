import nltk
from nltk.corpus import stopwords
from nltk.stem.porter import *
import string
import os
import sys

#dictionary that map term document with the list of document in which they appears
document = {}

#list of all document Id in the corpus
listDocument = []

# handle for parenthesis
handle = 0

# list to store operators in the evaluation process
operator = []

#final value computed
value = []


#utility function  that initialize that create a list of document Id
# after that it initialiaze each term with the list of document in which it occurs
def initialiazeDocument(input):
    for i in range(1, 1401):
        listDocument.append(i)
    with open(input) as documents:
        for line in documents:
            val = line.strip().split("=>") 
            if len(val) == 1:
                continue
            else:
                key = val[0].translate(None, "'")
                key = key.replace(" ", "")
                value = val[1]
                doc = createListDoc(value)
                document[key]= doc

#utility function that create a list of document of each term in the corpus
def createListDoc(input):
    result = []
    s = input.translate(None, '[()]')
    s = s.split(',')
    for i in s:
        result.append(int(i))

    result.pop(0)
    res = result[::2]
    return res

#utility function that clean the input text: lower, tokenize, stem
def cleanInput(input):
    stemmer = PorterStemmer()
    lowers = input.lower()
    no_punctuation = lowers.translate(None, '!"#$%&\'*+,-./:;<=>?@[\\]^_`{|}~')
    tokens = nltk.word_tokenize(no_punctuation)
    newtokens = [w for w in tokens if not w in stopwords.words('english')]
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed

#utility function that evaluate the input 
def evaluate(input):
    handle = 0
    for item in input:
        if item is "(":
           handle += 1
        elif item is ")":
            handle -= 1
        elif item in ("not", "and", "or"):
            operator.append(item)
        else:
            evaluateItem(item)
    if handle != 0:
        sys.exit("invalid expression the number of braket have to match")

    while operator:
        op  = operator.pop()
        if op == "not":
            evaluateNot()
        elif op == "and":
            evaluateAnd()
        elif op == "or":
            evaluateOr()

#Or evaluator
def evaluateOr():
    try:
        newDoc1 = value.pop()
        newDoc2 = value.pop()
        res = list(set(newDoc1) | set(newDoc2))
        value.append(res)
    except:
        sys.exit("Your expression is invalid Or must have two operand expression")

# And Evaluator
def evaluateAnd():
    try:
        newDoc1 = value.pop()
        newDoc2 = value.pop()
        res = list(set(newDoc1) & set(newDoc2))
        value.append(res)
    except:
        sys.exit("Your expression is invalid And must have two operand expression")

#Not evaluator
def evaluateNot():
    try:
        val = value.pop()
        print listDocument
        res = list(set(listDocument) - set(val))
        value.append(res)
    except:
        sys.exit("Your expression is invalid Not must have an expression")


#Item evaluator
def evaluateItem(item):
   value.append(document[item])

#utility function for command line interaction
def commandLine():
    run = True
    while run:
        print 'enter the exprerssion to search or quit to quit'
        search = raw_input('Enter the expression to search: ').strip()
        if search == 'quit':
            run = False
        else:
            s = cleanInput(search)
            evaluate(s)
            print "the result of your search is: "
            for i in value:
                print  "The expression appears in " + str(len(i)) + " documents" 
                print sorted(i)
            del value[0:len(value)]

#the main function
def main():
    filename = os.path.abspath(sys.argv[1])
    initialiazeDocument(filename)
    commandLine()
   
   
if __name__ == "__main__":
    main()













