

import nltk
from nltk.corpus import stopwords
from nltk.stem.porter import *
import string
import os
import math
import operator

#class for tf-idf
class TF_IDF:
    document = {}
    termFrequency = {}
    documentFrequency = {}
    inverseDocumentFrequency = {}

    #Constructor
    def __init__(self, input):
        with open(input) as documents:
            for line in documents:
                val = line.strip().split("=>")
                if len(val) == 1:
                    continue
                else:
                    key = val[0].translate(None, "'")
                    key = key.replace(" ", "")
                    value = val[1]
                    self.document[key] = value
                    doc = self.createListDoc(value)
                    self.documentFrequency[key] = doc.pop(0)
                    self.termFrequency[key] = zip(doc[::2], doc[1::2])
        for item in self.documentFrequency:
            self.inverseDocumentFrequency[item] = self.idf(item)

    #utility function that create a list of document
    def createListDoc(self, input):
        result = []
        s = input.translate(None, '[()]')
        s = s.split(',')
        for i in s:
            result.append(int(i))
        return result
    #utility function that compute the idf
    def idf(self, term):
        try:
            return 1 + float(math.log(1400/self.documentFrequency[term]))
        except:
            return 1

    #utility function that get the document list
    def getDocument(self):
        return self.document

    #utility function that return the term frequency
    def getTermFrequency(self):
        return self.termFrequency

    #utility function that return the document frequency
    def getDocumentFrequency(self):
        return self.documentFrequency

    #utility function that return the inverse document frequency
    def getIdf(self):
        return self.inverseDocumentFrequency

#class that process the input query
class ProcessQuery:

    stemmedQuery = []
    tfQuery = {}
    def __init__(self, input):
        self.stemmedQuery = self.cleanInput(input)
        self.termQuery(self.stemmedQuery)

    def cleanInput(self, input):
        stemmer = PorterStemmer()
        lowers = input.lower()
        no_punctuation = lowers.translate(None, string.punctuation)
        tokens = nltk.word_tokenize(no_punctuation)
        newtokens = [w for w in tokens if not w in stopwords.words('english')]
        stemmed = []
        for item in tokens:
            stemmed.append(stemmer.stem(item))
        return stemmed

    def termQuery(self, query):
        for item in query:
            try:
                self.tfQuery[item] += 1
            except:
                self.tfQuery[item] = 1
            for item in self.tfQuery:
                self.tfQuery[item] = 1 + float(math.log(self.tfQuery[item]))

    def getStemmedQuery(self):
        return self.stemmedQuery
    def gettfQuery(self):
        return self.tfQuery

#class to compute the tf-idf
class computeTF_IDF:

    #Contructor
    def __init__(self, query, documents):
        self.query= query
        self.documents = documents
        self.score = {}
        self.sortedScore = []
        self.outScore = []

    #get Term
    def getTerm(self):
        return self.documents.getTermFrequency()

    #Compute cosine
    def cosine(self):
        queryTerm  = self.query.gettfQuery()
        documentTerm = self.documents.getTermFrequency()
        for item in queryTerm:
                doc = documentTerm[item]
                for term in doc:
                    try:
                        self.score[term[0]] += self.tf_idf(item, term[0])*queryTerm[item]
                    except:
                        self.score[term[0]] = self.tf_idf(item, term[0])*queryTerm[item]
        self.sortedScore = sorted(self.score.iteritems(), key=operator.itemgetter(1), reverse=True)

    #get Query
    def getQuery(self):
        return self.query.gettfQuery()
    #getScore
    def getScore(self):
        return self.score
    #Compute TF-IDF
    def tf_idf(self, term, doc):
        return self.tfCal(term, doc)*self.documents.getDocumentFrequency()[term]

    #Compute TF
    def tfCal(self, term, doc):
        try:
            newTerm = self.documents.getTermFrequency()[term]
            for item in newTerm:
                if item[0] == doc:
                    return 1 + float(math.log(item[1]))
            return 1
        except:
            return 1
    #get the top K result
    def topKResult(self, k):
        return self.sortedScore[:k]

#Get the input
def getinput(input):
    with open(input) as documents:
        pattern = re.compile('.I\s\d+')
        textDoc = []
        tmpDoc = ''
        for line in documents:
            m = pattern.match(line.strip())
            if line.strip() == '.I 1':
                continue
            elif m:
                textDoc.append(tmpDoc)
                tmpDoc = ''
            else:
                tmpDoc  = tmpDoc + " " + line.strip()
    textDoc.append(tmpDoc)
    return textDoc

#write array in ouput
def writeArrayOutput(input):
        relevance = 0.0
        file = open( "Result" + ".txt", "w+")
        file.write("The Result output the top 10 similar document to the query \n the format is the following: Query Id : [(DocumentId, tf-idf-score)...])\n\n")
        for element in input:
            for item in element:
                if item[0] == input.index(element) + 1:
                    relevance = 1.0
            precision = relevance/1.0
            recall  = relevance/10.0
            file.write( "Query " + str(input.index(element) + 1) + " : " + str(element) + "\n")
            file.write("Precision and Recall for Query " + str(input.index(element) + 1) + ":" + "precision: " +  str(precision) + ", recall: " + str(recall) + "\n\n")
            relevance = 0.0

        file.close()

#main function
def main():
    output = []
    q = os.path.abspath(sys.argv[2])
    textDoc = getinput(q)
    filename = os.path.abspath(sys.argv[1])
    v = TF_IDF(filename)
    for element in textDoc:
        query = ProcessQuery(element)
        cmp = computeTF_IDF(query, v)
        cmp.cosine()
        output.append(cmp.topKResult(10))
    writeArrayOutput(output)


if __name__== "__main__":
    main()